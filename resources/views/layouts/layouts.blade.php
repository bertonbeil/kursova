<!DOCTYPE html>
<html>
    <head>
      <title>DS.Resort | {{$title}}</title>
      <link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon">
      <link rel="icon" href="images/favicon.ico" type="image/x-icon">
      <link rel="stylesheet" href="/css/main.css">
      <link rel="stylesheet" href="/css/media.css">
      <link rel="stylesheet" href="/css/style.css">
      <link rel="stylesheet" href="/css/module.css">
      <link rel="stylesheet" href="/css/dateTimePicker.css">
      <link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
      <!-- Latest compiled and minified CSS -->
      <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
      <!-- Optional theme -->
      <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">

    </head>
    <body>
      <nav class="navbar ">
        <div class="container">
          <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
              <span class="glyphicon glyphicon-menu-hamburger" aria-hidden="true"></span>
            </button>
              <a class="navbar-brand logo" href="/">DS.Resort</a>
          </div>
          <div id="navbar" class="navbar-collapse collapse">
            <ul class="nav navbar-nav">
              <li class="active"><a href="/">Головна</a></li>
              <li><a href="/about">Про нас</a></li>
              <li><a href="/news">Новини</a></li>
              <li><a href="/contacts">Контакти</a></li>
            </ul>

            <ul class="nav navbar-nav navbar-right">
              @if (Auth::check())
                <li>
                  <!-- <a class="btn btn-reg" href="/logout" role="button">Вийти</a> -->
                  <div class="dropdown">
                    <button class="btn btn-dropdown dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                      <div class="drop-image" style="background: url(/images/{{ isset(Auth::user()->avatar) ? Auth::user()->avatar : user-icon.png }}) no-repeat;background-size: cover;">

                      </div>
                      <div class="drop-text">
                        <span>{{ isset(Auth::user()->name) ? Auth::user()->name : Auth::user()->email }}</span>
                        <span class="caret"></span>
                      </div>

                    </button>
                    <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
                      <li><a href="/my-details">Мої дані</a></li>
                      <li role="separator" class="divider"></li>
                      <li><a href="/logout">Вихід</a></li>
                    </ul>
                  </div>
                </li>
              @else
              <li><a class="btn btn-reg" href="/register" role="button">Зареєструватись</a></li>
              <li><a class="btn btn-login" href="/login" role="button"> <span class="glyphicon glyphicon-user" aria-hidden="true"></span>Увійти</a></li>
              @endif
            </ul>

          </div><!--/.nav-collapse -->
        </div><!--/.container-fluid -->
      </nav>
      @yield('content')
      <footer>
        <div class="container">
          <div class="row">
            <div class="col-md-3 col-sm-12 col-xs-12 center">
              <div class="dib">
                <a class="navbar-brand logo" href="/">DS.Resort</a>
              </div>
            </div>
            <div class="col-md-6 col-sm-12 col-xs-12 center">
              <div class="foot-menu">
                <ul class="nav navbar-nav">
                  <li class="active"><a href="/">Головна</a></li>
                  <li><a href="/about">Про нас</a></li>
                  <li><a href="/news">Новини</a></li>
                  <li><a href="/contacts">Контакти</a></li>
                </ul>
              </div>
            </div>
            <div class="col-md-3 col-sm-12 col-xs-12 center">
              <div class="social">
                <a href="#"><img src="/images/facebook.png" width="25" alt=""></a>
              </div>
              <div class="social">
                <a href="#"><img src="/images/google+.png" width="25" alt=""></a>
              </div>
              <div class="social">
                <a href="#"><img src="/images/instagram.png" width="25" alt=""></a>
              </div>
              <div class="social">
                <a href="#"><img src="/images/twitter.png" width="25" alt=""></a>
              </div>
              <div class="social">
                <a href="#"><img src="/images/vk.png" width="25" alt=""></a>
              </div>
            </div>
          </div>
        </div>
      </footer>
    </body>
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
  <script src="/js/dateTimePicker.js"></script>
  <script src="/js/date.format.js"></script>
  <script src="/js/main.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
</html>
