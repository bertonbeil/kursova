@extends('layouts.layouts')
@section('content')
<div class="content">
    <div class="container">
      <div class="row mt50 mb20">
        <div class="col-md-12">
          <a class="btn btn-success" onclick="window.history.back()" >Назад</a>
          <form class="" action="" method="post" enctype="multipart/form-data">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">

            <div class="form-group">
              <label for="pwd">Зображення:</label>
              <input type="file" class="form-control" id="pwd" name="image[]"  multiple>
            </div>
            <button type="submit" class="btn btn-default">Додати</button>
          </form>
        </div>
      </div>
    </div>
  </div>
@endsection
