@extends('layouts.layouts')
@section('content')
<div class="content">
    <div class="container">
      <div class="row mt50 mb20">
        <div class="col-md-12">
          <form class="" action="add" method="post" enctype="multipart/form-data">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <div class="form-group">
              <label for="title">Назва номеру</label>
              <input type="text" class="form-control" id="title" name="title" required>
            </div>
            <div class="form-group">
              <div class="col-md-4">
                <label for="title">Площа номеру</label>
                <input type="number" class="form-control" id="title" name="area" required>
              </div>
              <div class="col-md-4">
                <label for="title">Тип ліжка</label>
                <input type="text" class="form-control" id="title" name="bed" required>
              </div>
              <div class="col-md-4">
                <label for="title">Ціна</label>
                <input type="number" class="form-control" id="title" name="room_price" required>
              </div>
            </div>


            <div class="form-group">
              <label for="pwd">Зображення:</label>
              <input type="file" class="form-control" id="pwd" name="image" required>
            </div>
            <div class="form-group">
              <label for="pwd">Опис:</label>
              <textarea name="description" id="bodyField" class="form-control" rows="8" cols="80" required></textarea>
              @ckeditor('bodyField')
            </div>
            <button type="submit" class="btn btn-default">Додати</button>
          </form>
        </div>
      </div>
    </div>
  </div>
@endsection
