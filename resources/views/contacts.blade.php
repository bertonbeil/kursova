@extends('layouts.layouts')
@section('content')
  <div class="content">
  <div class="container">
    <div class="row">
      <div class="col-md-6 img-back">
        <h1>DS.Resort</h1>
      </div>
      <div class="col-md-6">
        <div class="contact-content">
          <p>79066,УКРАЇНА, ЛЬВІВ, ВУЛ.МАСАЧУСЕЦЬКА, 14</p>
            <p>  ТЕЛ. +38 (032) 232 – 50 – 00 (ЦІЛОДОБОВО)</p>
            <p>  ФАКС. +38 (032) 232 – 50 – 15 (ЦІЛОДОБОВО)</p>
            <p>  МОБ. +38 (093) 314 – 30 – 18 (ЦІЛОДОБОВО)</p>
            <p>  INFO@DS.RESORT.COM.UA</p>
            <div class="map">
              <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d69362.00769464775!2d13.086315291139762!3d49.754445135283625!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x470a92011ecf2845%3A0xb55435b0998345e6!2zw5psaWNlIDYxLCAzMzAgMzMgw5psaWNlLCDQp9C10YXRltGP!5e0!3m2!1suk!2sua!4v1490004929553"  height="400" frameborder="0" style="border:0" allowfullscreen></iframe>
            </div>
            <p>З ПИТАНЬ БРОНЮВАННЯ ТА КОНФЕРЕНЦ-СЕРВІСУ:</p>
            <p>ТЕЛ. +38 (032) 232 – 50 – 05 (ПН-ПТ) 09:00-19:00</p>
            <p>МОБ. +38 (093) 314 – 30 – 49 (ПН-ПТ) 09:00-19:00</p>
            <p>МОБ. +38 (093) 314 – 30 – 19 (ПН-ПТ) 09:00-19:00</p>

        </div>
      </div>
    </div>
  </div>
  </div>
@endsection
