@extends('layouts.layouts')
@section('content')
  <div class="content">
    <div class="img-wrap">
      <img src="images/pic.png" alt="">
      <div class="form">
        <div class="form-content">
          <div class="icon">
            <img src="images/user.png" alt="">
          </div>
          <div class="form-title">
            <p>Вхід</p>
          </div>
          <div class="form-input">
            <form action="{{url('/login')}}" method="POST" autocomplete="off">
              {{csrf_field()}}
              <input type="text" name="login" placeholder="Ваш логін">
              @if($errors->has('login'))
                <span>
                  <strong>{{$errors->first('login')}}</strong>
                </span>
              @endif
              <input type="password" name="password" placeholder="Пароль">
              @if($errors->has('name'))
                <span>
                  <strong>{{$errors->first('name')}}</strong>
                </span>
              @endif
              <button type="submit"  class="form-button" name="button">Розпочати</button>
            </form>
          </div>
        </div>

      </div>
    </div>
  </div>
@endsection
