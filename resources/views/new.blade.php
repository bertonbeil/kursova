@extends('layouts.layouts')
@section('content')
<div class="content">
    <div class="container">
      <div class="row mt50 mb20">
        @if($news)
        <div class="col-md-12">
          <div class="news-wrap">
            <p>{{$news->title}}</p>
            <span class="glyphicon glyphicon-calendar center" aria-hidden="true"><span>{{$news->created_at->format('d.m.Y')}}</span></span>
            <img src="/images/upload/{{$news->image}}" alt="">
            <p class="last-news-p">{{$news->description}}</p>
          </div>
        </div>
        @endif
      </div>
    </div>
  </div>
@endsection
