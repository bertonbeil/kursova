<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/','HomeController@showHomePage');

Route::get('/about', function () {
    return view('about')->with('title','Про нас');
});
// Route::get('/news', function () {
//     return view('news')->with('title','Новини');
// });
Route::get('/news','NewsController@showNews');
Route::get('/add-news','NewsController@addNews');
Route::post('/add-news','NewsController@getNews');
Route::get('/news/{id}','NewsController@showNew');
Route::get('/delete-news/{id}','NewsController@deleteNews');
Route::get('/contacts', function () {
    return view('contacts')->with('title','Контакти');
});
// Route::get('/room', function () {
//     return view('room')->with('title','Кімната');
// });
Route::group(['prefix' => 'room'],function ()
{
    Route::get('/add','HomeController@addRoom');
  Route::get('/{id}','HomeController@showRoom');

  Route::post('/add','HomeController@getRoom');
  Route::get('/delete/{id}','HomeController@deleteRoom');
  Route::get('/edit/{id}','HomeController@editShowRoom');
  Route::post('/edit/{id}','HomeController@editGetRoom');
  Route::get('/edit/{id}/photo','HomeController@photoRoom')->name('photo');
  Route::get('/edit/{id}/photo/add','HomeController@addPhotoRoom');
  Route::get('/edit/{id}/photo/delete/{photo}','HomeController@deletePhotoRoom');
  Route::post('/edit/{id}/photo/add','HomeController@getPhotoRoom');


});

Route::get('/reserve/{id}','ReserveController@showReserve');
Route::post('/reserve','ReserveController@getReserve');

Route::get('/delete-reserve/{id}','DetailController@deleteReserve');
Route::get('/delete-testimonial/{id}','DetailController@deleteTestimonial');


Route::post('/add-testimonials','AddTestimonialsController@getTestimonial');



Route::get('/login','Auth\AuthController@showLoginForm');
Route::post('/login','Auth\AuthController@login');
Route::get('/register','Auth\AuthController@showRegisterForm');
Route::post('/register','Auth\AuthController@create');
Route::get('/logout','Auth\AuthController@logout');

Route::get('/my-details','DetailController@myDetails');




//
