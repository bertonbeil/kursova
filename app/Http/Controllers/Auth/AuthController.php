<?php

namespace App\Http\Controllers\Auth;

use App\User;
use Validator;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Http\Request;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;

class AuthController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Registration & Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users, as well as the
    | authentication of existing users. By default, this controller uses
    | a simple trait to add these behaviors. Why don't you explore it?
    |
    */

    use AuthenticatesAndRegistersUsers, ThrottlesLogins;

    /**
     * Where to redirect users after login / registration.
     *
     * @var string
     */
     protected $loginView;
     protected $registerView;
     protected $username = 'login';

    protected $redirectTo = '/';

    /**
     * Create a new authentication controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware($this->guestMiddleware(), ['except' => 'logout']);
        $this->loginView='login';
        $this->registerView='register';
    }
    public function showLoginForm()
    {
        $view = property_exists($this, 'loginView')
                    ? $this->loginView : '';

        if (view()->exists($view)) {
            return view($view)->with('title','Вхід');
        }

        abort(404);
    }
    public function showRegisterForm()
    {
        $view = property_exists($this, 'registerView')
                    ? $this->registerView : '';

        if (view()->exists($view)) {
            return view($view)->with('title','Реєстрація');
        }

        abort(404);
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|max:255',
            'email' => 'required|email|max:255|unique:users',
            'password' => 'required|min:6|confirmed',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return User
     */
    protected function create(Request $request)
    {
      $data = $request->all();
      $validator = Validator::make($request->all(), [
        'email' => 'required|unique:users',
        'name' => 'required',
        'login' => 'required|unique:users',
        'password' => 'required',
        ]);
    if ($validator->fails()) {
            return redirect('/register')
                        ->withErrors($validator)
                        ->withInput();
        }

        else{
          $create=User::create([
            'name' => $request['name'],
            'email' => $request['email'],
            'remember_token' => $request['_token'],
            'login' => $request['login'],
            'password' => bcrypt($request['password']),
          ]);
          return redirect('/login')->with(['create'=>$create,'title'=>'fsdf']);
        }

    }
}
