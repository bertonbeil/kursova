<?php

namespace App\Http\Controllers;
use Auth;
use Crypt;
use App\Room;
use App\Photo;
use App\Testimonial;
use App\Http\Requests;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('home');
    }
    public function showHomePage()
    {
      $rooms=Room::orderBy('id','desc')->get();
      $testimonials=Testimonial::orderBy('id','desc')->take(1)->get();
      return view('home')->with(['title'=>'Головна','rooms'=>$rooms,'testimonials'=>$testimonials]);
    }
    public function showRoom($id)
    {
      # code...
      $rooms=Room::where('id',$id)->first();
      $photos=Photo::where('room_id',$id)->get();
      $testimonials=Testimonial::where('room_id',$id)->orderBy('id','desc')->take(3)->get();
      return view('room')->with(['title'=>'Кімната '.$id,'rooms'=>$rooms,'testimonials'=>$testimonials,'photos'=>$photos]);
    }
    public function addRoom()
    {
      if (Auth::check()) {
        # code...
        return view('add-room')->with(['title'=>'Додати кімнату']);
      }
      return view('404')->with(['title'=>'Помилка']);
    }
    public function getRoom(Request $request)
    {
      $file = $request->file('image');
      $name = time(). $file->getClientOriginalName();
      $file->move('images/upload', $name);
      $data = array_merge(['photo' => "/images/upload/{$name}"], $request->all());
      $articles=new Room;
       $articles->room_name=$request['title'];
       $articles->area=$request['area'];
       $articles->bed=$request['bed'];
       $articles->room_price=$request['room_price'];
       $articles->description=$request['description'];
       $articles->room_image=$name;
       $articles->save();
      return redirect('my-details')->with(['title'=>'Moї дані','status'=>'Кімната успішно добавлена']);
    }
    public function deleteRoom($id)
    {
      # code...
      if(Auth::user()->role === 'admin')
      {
       Room::destroy($id);
       return redirect('my-details')->with('status', 'Кімната видалено успішно');
     }
     return redirect('/');
    }
    public function editShowRoom($id)
    {
      if(Auth::user()->role === 'admin'){
        $room=Room::where('id',$id)->first();
        return view('edit-room')->with(['title'=>'Редагування кімнати', 'room'=>$room]);
      }
      return redirect('/');
    }
    public function editGetRoom(Request $request)
    {
      # code...
      $id=$request['id'];
      $articles=Room::find($id);
      $articles->room_name=$request['title'];
      $articles->area=$request['area'];
      $articles->bed=$request['bed'];
      $articles->room_price=$request['room_price'];
      $articles->description=$request['description'];
      if (!empty($request['image'])) {
        $file = $request->file('image');
        $name = time(). $file->getClientOriginalName();
        $file->move('images/upload', $name);
        $data = array_merge(['photo' => "/images/upload/{$name}"], $request->all());
        $articles->room_image=$name;
      }
      $articles->save();
       return redirect('my-details')->with('status', 'Кімната редаговано успішно');
    }
    public function photoRoom($id)
    {
      $photo=Photo::where('room_id',$id)->get();
      return view('photo')->with(['title'=>'Галерея','photos'=>$photo,'id'=>$id]);
    }
    public function addPhotoRoom($id)
    {
      # code...
      return view('add-photo')->with('title','Додавання');
    }
    public function getPhotoRoom(Request $request,$id)
    {
      # code...
      $photos = $request->file('image');
      foreach ($photos as $photo) {
        # code...
        $name = time(). $photo->getClientOriginalName();
        $photo->move('images/upload', $name);
        $articles=new Photo;
        $articles->image=$name;
        $articles->room_id=$id;
        $articles->save();
      }
      return redirect('room/edit/'.$id.'/photo')->with('status','Фотографії добавленні');
    }
    public function deletePhotoRoom($id,$photo)
    {
      # code...
      if(Auth::user()->role === 'admin')
      {
       Photo::destroy($photo);
       return back()->with('status', 'Картинка видалено успішно');
     }
     return redirect('/');
    }

}
